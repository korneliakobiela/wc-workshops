import { Component, Prop, h, State, Listen } from '@stencil/core';


@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: true
})
export class MyComponent {
  @Prop() header: string;

  @State() open: boolean;

  @Listen('click', { capture: true })
  handleClick() {
    console.log(this);
    this.open = !this.open;
  }

  render() {
    return <div>
      <header>{this.header}</header>
      <div hidden={this.open}><slot/></div>
    </div>;
  }
}
