## Web Components Driven development
1. Webpage is easier to built bottom-up than from the top to bottom
2. Types of component:
	- Statetles - dumb component
	- Stateful component - smart component with logic
	- Routed component - smart component 

3. Component are independent part of code

## WC API
### Templates
- Native WEB elements doesn't provide descriptive templates layout
``` html
<template></template>
```
- No descrptive deta binding
- Lazy loading mechanism

``` js
// Cloning template content
const el = document.querySelector('template');
conts clone = document.importNode(el.content, {deep:true})
```
- slot - allow us to inject content to holes in template
``` html
<template>
	<div>
		<slot name="header"></slot>
	</div>
</template>
```
### ES Modules
- Only for `script[type=module]`
- Allow to reuse JS code

``` js
import moduleName from 'module';
```
### Custom Elements
Allows us to have encapsulated reusable elements builded with native browser technology
- Creation of Custom Elements

``` js
customElements.define('name-element', class <XD> extend HtmlElement)
```
- Usage
``` html
<name-element></name-element>
```
``` js
document.createElement('name-element')
```

### Shadow DOM
- Encapsulate DOM parts
- Allow to use scope binded classes
``` js
const element = document.querySelector('div')
element.attachShadow({mode: 'open'})
```
## State of Components

1. Still not recommendation
2. Libraries
   - Stencil
   - Lit-Components
   - Slim.js
